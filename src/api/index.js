//引入请求模块
import http from '@/utils/http.js'
import user from './sysuser.js'
import dict from './dict.js'
import upload from './upload.js'
import role from './role.js'

//将多个请求对象进行合并
const api = Object.assign({},user,dict,upload,role)

export default api;
