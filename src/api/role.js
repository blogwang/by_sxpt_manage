//引入请求模块
import http from '@/utils/http.js'

export default {
  //保存角色
  saveRole(data) {
    return http.post("/api/role/save", data);
  },
  //修改角色
  updateRole(data) {
    return http.put("/api/role/update", data);
  },
  //查询角色
  queryRole(data) {
    return http.post("/api/role/query", data);
  },
  //删除角色
  deleteRole(id) {
    return http.delete("/api/role/delete/" + id);
  },
  //查询所有角色
  queryallRole() {
    return http.get("/api/role/queryall", {});
  }
}
