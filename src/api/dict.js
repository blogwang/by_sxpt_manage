//引入请求模块
import http from '@/utils/http.js'

//封装所有的请求方法
export default {

  //字典管理


  //保存字典
  saveDict(data) {
    return http.post("/api/dict", data);
  },
  //查询字典列表
  queryDict(data) {
    return http.post("/api/dict/query", data);
  },
  //删除字典
  deleteDict(id) {
    return http.delete("/api/dict/" + id);
  },
  //修改字典
  updateDict(data) {
    return http.put("/api/dict", data);
  },
  //查询分组
  queryDictGroup() {
    return http.get("/api/dict/group", {});
  },
  // 根据parentId删除相应的字典项
  deleteDictByParentId(pid) {
    // return;
    return http.delete("/api/dictByParentId/" + pid);
  },
  //检查编码是否唯一
  checkDictCode(code) {
    return http.get("/api/dict/checkCode",code);
  }


}
