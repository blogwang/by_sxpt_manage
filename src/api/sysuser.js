//引入请求模块
import http from '@/utils/http.js'

export default {

  // 保存用户
  saveUser(data) {
    return http.post("/api/user/save", data);
  },
  //查询用户
  queryUser(data) {
    return http.post("/api/user/query", data);
  },
  //修改用户
  updateUser(data) {
    return http.post("/api/user/update", data);
  },
  //检查账号是否唯一
  checkAccount(params) {
    return http.get("/api/user/check", params);
  },
  //删除用户
  deleteUser(id) {
    return http.delete("/api/user/delete/" + id);
  },
  //更改用户状态值status
  changeUserStatus(data) {
    return http.post("/api/user/changeStatus", data);
  },
  //重置密码
  resetpwd(params){
    return http.get("/api/user/resetpwd",params);
  }


}
