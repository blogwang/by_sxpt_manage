import axios from 'axios'
import {
  MessageBox,
  Message
} from 'element-ui'
import store from '@/store'
import {
  getToken
} from '@/utils/auth'

import host from './env.js'
// create an axios instance
const service = axios.create({
  baseURL: host, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000, // request timeout
  validateStatus: function(status) {
    return status >= 0; // default
  }
})

// request interceptor
service.interceptors.request.use(
  config => {
    // do something before request is sent

    if (store.getters.token) {
      // let each request carry token
      // ['X-Token'] is a custom headers key
      // please modify it according to the actual situation
      config.headers['X-Token'] = getToken()
    }
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
   */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    if (response.status == 200) {
      let rs = response.data;
      if (rs.code == 200) {
        return Promise.resolve(rs);
      } else {
        let errs = rs.data;
        let msg;
        if (errs) {
          let info = [];
          for (let e in errs) {
            info.push(errs[e]);
          }
          msg = info.join(" ");
        } else {
          msg = rs.msg;
        }
        //弹框提示
        Message({
          message: msg,
          type: 'error',
          duration: 3 * 1000
        })
        return Promise.resolve(rs);
      }

    } else {
      Message({
        message: "服务器请求失败",
        type: 'error',
        duration: 3 * 1000
      })
      return Promise.resolve({
        code: 0,
        msg: "服务器请求失败"
      });
    }
  },
  error => {}
)

export default {

  post(url, data) {
    return service.post(url, data);
  },

  get(url, params) {
    return service.get(url, {
      params
    });
  },

  put(url, data) {
    return service.put(url, data);
  },

  delete(url) {
    return service.delete(url);
  }

}
