/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const sysRouter = {
  path: '/sys',
  component: Layout,
  redirect: 'noRedirect',
  name: 'sys',
  meta: {
    title: '系统管理',
    icon: 'component'
  },
  children: [{
      // 此路径是地址栏的映射路径，输入此路径，显示下面component对应的路径资源
      path: 'user',
      component: () => import('@/views/sys/user/index'),
      name: 'user',
      meta: {
        title: '用户管理'
      }
    },
    {
      // 此路径是地址栏的映射路径，输入此路径，显示下面component对应的路径资源
      path: 'role',
      component: () => import('@/views/sys/role/index'),
      name: 'role',
      meta: {
        title: '角色管理'
      }
    },
    {
      // 此路径是地址栏的映射路径，输入此路径，显示下面component对应的路径资源
      path: 'dict',
      component: () => import('@/views/sys/dict/index'),
      name: 'dict',
      meta: {
        title: '字典管理'
      }
    }
  ]
}

export default sysRouter
